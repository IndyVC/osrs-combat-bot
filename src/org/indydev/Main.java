package org.indydev;

import org.indydev.Tasks.Prepare;
import org.indydev.Tasks.Random;
import org.indydev.Tasks.Train;
import org.rspeer.runetek.api.component.tab.Skill;
import org.rspeer.runetek.api.component.tab.Skills;
import org.rspeer.runetek.api.movement.position.Area;
import org.rspeer.runetek.api.movement.position.Position;
import org.rspeer.runetek.api.scene.Players;
import org.rspeer.runetek.event.listeners.NpcSpawnListener;
import org.rspeer.runetek.event.listeners.RenderListener;
import org.rspeer.runetek.event.types.NpcSpawnEvent;
import org.rspeer.runetek.event.types.RenderEvent;
import org.rspeer.script.ScriptMeta;
import org.rspeer.script.task.Task;
import org.rspeer.script.task.TaskScript;

import java.awt.*;

@ScriptMeta(name = "CombatTrainer", developer = "Indy",desc = "Trains all melee stats. 1-15 at goblins, 15-20 at cows, 20-30 at big frogs. (Will improve this to higher levels aswell). REQS: Have mithril scimi, platebody/chain, legs in bank.")
public class Main extends TaskScript implements RenderListener, NpcSpawnListener {

    private static final Task[] TASKS = {new Prepare(), new Train(), new Random()};
    public static Area combatTutors = Area.rectangular(new Position(3214,3242), new Position(3223,3237));
    public static Area goblins = Area.rectangular(new Position(3240,3251),new Position(3258,3230));
    public static Area cows = Area.rectangular(new Position(3245,3294), new Position(3263,3278));
    public static Area lumbridgeBank = Area.rectangular(new Position(3207,3220,2),new Position(3210,3218,2));
    public static Area bigFrogs = Area.rectangular(new Position(3200,3197), new Position(3212,3188));


    @Override
    public void notify(RenderEvent renderEvent) {
        Graphics2D render = (Graphics2D)renderEvent.getSource();
        render.drawRect(0,0,500,500);
        render.drawString(String.format("Current combat level: %s",Players.getLocal().getCombatLevel()),10,10);
        render.drawString(String.format("Current attack level: %s", Skills.getCurrentLevel(Skill.ATTACK)),10,30);
        render.drawString(String.format("Current strength level: %s", Skills.getCurrentLevel(Skill.STRENGTH)),10,50);
        render.drawString(String.format("Current defence level: %s", Skills.getCurrentLevel(Skill.DEFENCE)),10,70);
    }

    @Override
    public void onStart() {
        submit(TASKS);
    }

    @Override
    public void notify(NpcSpawnEvent npcSpawnEvent) {
        if(npcSpawnEvent.getSource().getName().contains("Wise")){
            npcSpawnEvent.getSource().interact("Dismiss");
        }
    }
}
