package org.indydev.Helpers;

import org.rspeer.runetek.adapter.component.Item;
import org.rspeer.runetek.api.commons.Time;
import org.rspeer.runetek.api.component.tab.EquipmentSlot;
import org.rspeer.runetek.api.component.tab.Inventory;
import org.rspeer.runetek.api.component.tab.Skill;
import org.rspeer.runetek.api.component.tab.Skills;
import org.rspeer.ui.Log;

public class PlayerHelper {

    public static boolean hasItem(String weapon, EquipmentSlot slot){
        Item[] weapons = Inventory.getItems(i->i.getName().equals(weapon));
        boolean hasWeapon = false;

        for(Item item:weapons){
            if(item.getName().contains(weapon)){
                hasWeapon = true;
                wearItem(slot,weapon);
            }
        }

        if(slot.getItem()!=null && slot.getItem().getName().contains(weapon)){
            hasWeapon = true;
        }
        return hasWeapon;
    }

    public static void wearItem(EquipmentSlot slot, String item){
        Item[] its = Inventory.getItems(i->i.getName().equals(item));
        Item it = its.length>0?its[0]:null;
        if((slot.getItem()==null || !slot.getItem().getName().contains(item) )&& item != null){
            it.interact(it.getActions()[0]);
            Time.sleep(600,1000);
            String msg = String.format("Equips %s",item);
            Log.fine(msg);
        }
    }

    public static boolean combatLevelsAreUnder(int level){
        return Skills.getCurrentLevel(Skill.ATTACK)<=level && Skills.getCurrentLevel(Skill.STRENGTH) <=level && Skills.getCurrentLevel(Skill.DEFENCE)<=level;
    }
}
