package org.indydev.Helpers;

import org.rspeer.runetek.api.Game;
import org.rspeer.runetek.api.commons.Time;
import org.rspeer.runetek.api.component.Bank;
import org.rspeer.ui.Log;

public class BankHelper {

    public static void retrieveFromBank(String[] retrieve, String[] replacement){
        Bank.open();
        Time.sleepUntil(Bank::isOpen,300);
        Bank.depositInventory();
        Time.sleep(500,1000);
        Bank.depositEquipment();
        Time.sleep(500,1000);
        Log.fine("Banked everything.");
        for(int i=0;i<retrieve.length;i++){
            if(Bank.contains(retrieve[i]) || Bank.contains(replacement[i])) {
                if (Bank.withdraw(retrieve[i], 1)) {
                    Log.fine(String.format("Withdrawed %s", retrieve[i]));
                } else {
                    Log.fine(String.format("Item is not in bank, using replacement: %s", replacement[i]));
                    Bank.withdraw(replacement[i], 1);
                }
                Time.sleep(500, 1000);
            }else{
                Bank.close();
                Game.logout();
                Log.fine("No correct items in bank, closing.");
            }
        }
        Bank.close();
        Time.sleepWhile(Bank::isOpen,300);

    }
}
