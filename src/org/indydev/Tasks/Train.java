package org.indydev.Tasks;

import org.indydev.Helpers.PlayerHelper;
import org.indydev.Main;
import org.rspeer.runetek.adapter.scene.Npc;
import org.rspeer.runetek.api.Game;
import org.rspeer.runetek.api.commons.Time;
import org.rspeer.runetek.api.component.tab.*;
import org.rspeer.runetek.api.movement.Movement;
import org.rspeer.runetek.api.movement.position.Area;
import org.rspeer.runetek.api.scene.Npcs;
import org.rspeer.runetek.api.scene.Players;
import org.rspeer.runetek.event.listeners.DeathListener;
import org.rspeer.runetek.event.types.DeathEvent;
import org.rspeer.script.task.Task;
import org.rspeer.ui.Log;

import java.util.Arrays;

public class Train extends Task implements DeathListener {
    @Override
    public boolean validate() {
        return validator();
    }

    @Override
    public int execute() {

        Area cbArea = getCombatArea();
        String target = getTarget();
        if(!cbArea.contains(Players.getLocal().getPosition())) {
           runTo(cbArea);
        }else {
            attack(target);
        }
        checkExit();
        return 300;
    }

    private boolean validator(){
        if(PlayerHelper.combatLevelsAreUnder(20)){
            return (PlayerHelper.hasItem("Training sword",EquipmentSlot.MAINHAND)&& PlayerHelper.hasItem("Training shield",EquipmentSlot.OFFHAND));
        }else if(PlayerHelper.combatLevelsAreUnder(30)){
            return PlayerHelper.hasItem("Mithril scimitar",EquipmentSlot.MAINHAND) && (PlayerHelper.hasItem("Mithril platebody",EquipmentSlot.CHEST) || PlayerHelper.hasItem("Mithril chainbody",EquipmentSlot.CHEST))&& PlayerHelper.hasItem("Mithril platelegs",EquipmentSlot.LEGS);
        }
        return false;
    }

    private void runTo(Area area){
        if(!Movement.isRunEnabled() && Movement.getRunEnergy()>50){
            Movement.toggleRun(true);
        }
        Movement.walkToRandomized(area.getCenter());
        Log.fine(String.format("Walking to area (train)"));
        Time.sleepWhile(Players.getLocal()::isMoving,300);
    }

    private void attack(String npc){
        chooseAttackStyle();
        Npc target;
        if(Players.getLocal().getTarget()==null) {
            target = Npcs.getNearest(i -> i.getName().equals(npc) && !i.isHealthBarVisible() && Movement.isWalkable(i.getPosition()));
            target.interact(target.getActions()[0]);
            Log.fine(String.format("Attack %s",npc));
        }
    }

    private void chooseAttackStyle(){
        int[] levels = {Skills.getCurrentLevel(Skill.ATTACK), Skills.getCurrentLevel(Skill.STRENGTH), Skills.getCurrentLevel(Skill.DEFENCE)};
            int lowestLevel = Arrays.stream(levels).min().getAsInt();
            int lowestLevelIndex = -1;
            for (int i = 0; i < levels.length; i++) {
                if (lowestLevel == levels[i]) {
                    lowestLevelIndex = i;
                    break;
                }
            }

            switch (lowestLevelIndex) {
                case 0:
                    Combat.select(0);
                    break;
                case 1:
                    Combat.select(1);
                    break;
                case 2:
                    Combat.select(3);
            }

    }

    private Area getCombatArea(){
        if(PlayerHelper.combatLevelsAreUnder(15)){
            return Main.goblins;
        }else if(PlayerHelper.combatLevelsAreUnder(20)){
            return Main.cows;
        }else if(PlayerHelper.combatLevelsAreUnder(40)){
            return Main.bigFrogs;
        }
        return null;
    }

    private String getTarget(){
        if(PlayerHelper.combatLevelsAreUnder(15)){
            return "Goblin";
        }else if(PlayerHelper.combatLevelsAreUnder(20)){
            return "Cow";
        }else if(PlayerHelper.combatLevelsAreUnder(40)){
            return "Big frog";
        }
        return null;
    }

    private int checkExit(){
        if(!PlayerHelper.combatLevelsAreUnder(30)){
            Game.logout();
            Log.fine("Successfully executed the script. All melee stats are now 30.");
            return -1;
        }
        return 300;
    }

    @Override
    public void notify(DeathEvent deathEvent) {
        if(deathEvent.getSource().getTarget() == Players.getLocal().getTarget()){
            Log.fine(String.format("Target died"));
        };

    }
}
