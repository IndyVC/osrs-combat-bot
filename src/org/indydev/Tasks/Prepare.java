package org.indydev.Tasks;

import org.indydev.Helpers.BankHelper;
import org.indydev.Helpers.PlayerHelper;
import org.indydev.Main;
import org.rspeer.runetek.adapter.component.Item;
import org.rspeer.runetek.adapter.scene.Npc;
import org.rspeer.runetek.api.Game;
import org.rspeer.runetek.api.commons.Time;
import org.rspeer.runetek.api.component.Bank;
import org.rspeer.runetek.api.component.Dialog;
import org.rspeer.runetek.api.component.tab.*;
import org.rspeer.runetek.api.movement.Movement;
import org.rspeer.runetek.api.movement.position.Area;
import org.rspeer.runetek.api.scene.Npcs;
import org.rspeer.runetek.api.scene.Players;
import org.rspeer.script.task.Task;
import org.rspeer.ui.Log;

public class Prepare extends Task {

    @Override
    public boolean validate() {
        return validator();
    }


    @Override
    public int execute() {
        if(PlayerHelper.combatLevelsAreUnder(20)) {
            if (!Main.combatTutors.contains(Players.getLocal())) {
                runTo(Main.combatTutors);
            } else {
                Time.sleep(500, 1000);
                askTrainingWeapons("Melee combat tutor");
            }
        }else if(PlayerHelper.combatLevelsAreUnder(30)){
            if(!Main.lumbridgeBank.contains(Players.getLocal())){
                runTo(Main.lumbridgeBank);
            }else{
                Time.sleep(500,1000);
                BankHelper.retrieveFromBank(new String[]{"Mithril scimitar","Mithril platebody","Mithril platelegs"}, new String[]{"","Mithril chainbody",""});
            }
        }
        return 500;
    }

    private boolean validator(){
        if(PlayerHelper.combatLevelsAreUnder(20)){
            return !(PlayerHelper.hasItem("Training sword",EquipmentSlot.MAINHAND) && PlayerHelper.hasItem("Training shield",EquipmentSlot.OFFHAND));
        }else if(PlayerHelper.combatLevelsAreUnder(30)){
            return !(PlayerHelper.hasItem("Mithril scimitar",EquipmentSlot.MAINHAND) && (PlayerHelper.hasItem("Mithril platebody",EquipmentSlot.CHEST) ||PlayerHelper.hasItem("Mithril chainbody",EquipmentSlot.CHEST))&& PlayerHelper.hasItem("Mithril platelegs",EquipmentSlot.LEGS));
        }
        return false;
    }



    private void runTo(Area area){
        Movement.walkToRandomized(area.getCenter());
        Log.fine("Walking to area (fetch)");
        Time.sleepWhile(Players.getLocal()::isMoving,200);
    }

    private void askTrainingWeapons(String tutor) {
        if (!Dialog.isOpen()) {
            Npc npc = Npcs.getNearest(i -> i.getName().equals(tutor));
            Log.fine("Asking for weapons");
            if(tutor=="Melee combat tutor") {
                if(npc!=null)npc.interact(npc.getActions()[0]);
            }else{
                if(npc!=null)npc.interact(npc.getActions()[1]);
            }
        }
        while (Dialog.canContinue()) {
            Dialog.processContinue();
        }
        Time.sleep(500, 1000);
        Tabs.open(Tab.INVENTORY);
        if (PlayerHelper.hasItem("Training sword", EquipmentSlot.MAINHAND)) {
            PlayerHelper.wearItem(EquipmentSlot.MAINHAND, "Training sword");
            Time.sleep(400, 600);
        }
        if (PlayerHelper.hasItem("Training shield", EquipmentSlot.OFFHAND)) {
            PlayerHelper.wearItem(EquipmentSlot.OFFHAND, "Training shield");
            Time.sleep(400, 600);
        }
    }
}
